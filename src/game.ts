//////////////////////////////////////////////
// GameJam Fantasy Scene
// Sofia Alves and Carl Fravel
// Follow the sprite...
//////////////////////////////////////////////

import {BuilderHUD} from './modules/BuilderHUD'

/*
import { getUserData, getUserPublicKey } from "@decentraland/Identity"


const publicKeyRequest = executeTask(async () => {
  const publicKey = await getUserPublicKey()
  log("publicKey =", publicKey)
  return publicKey
})

class IDSystem implements ISystem {
  update() {
    if (publicKeyRequest.didFail) {
      log("error fetching id" + publicKeyRequest.error)
    } else {
      log("id:" + publicKeyRequest.result)
    }
  }
}

const userData = executeTask(async () => {
  const data = await getUserData()
  log("DisplayName =", data.displayName)
  return data.displayName
})
*/

let playMusic = true

//////////
// Magic Numbers

// Animation frame rate
const updateFPS = 30
const stoneAnimationFPS = 24

// Animation speeds
const floatingStoneAnimationSpeed = 1
const treesAnimationSpeed = 1
const grimReaperAnimationSpeed = 1
const dragonAnimationSpeed = 1
const girlAnimationSpeed = 1

// Animation durations in seconds
const girlAnimationDuration = 180/updateFPS/girlAnimationSpeed
const grimReaperAnimationDuration = 120/updateFPS/grimReaperAnimationSpeed
const dragonAnimationDuration = 140/updateFPS/dragonAnimationSpeed
const floatingStoneAnimationDuration = 250/stoneAnimationFPS/floatingStoneAnimationSpeed
const treesAnimationDuration = 250/updateFPS/treesAnimationSpeed

// Animation names
const floatingStoneAnimationName = "Float"
const treesAnimationName = "LeafSwing"
const grimReaperAnimationName = "AttackSwing"
const dragonAnimationName = "Attack"
const girlAnimationName = "Bow"

// Timers
let floatingStoneTimer = 0
let grimReaperTimer = 0
let dragonTimer = 0
let girlTimer = 0
let stoneClicked = false

// Sprite control
let spriteHeight = 2
let spriteHeightAmplitude = 0.125
let spriteHeightSpeed = 4
let spriteMovementFraction = 0
let spriteSpeed = 0.5
let spriteMoving = false


// Geofences (xMin, zMin, Xmax, Zmax) // todo turn these into invisible planes or triggers, with scene as root, or perform a one time rotation/translation on them based on scene's position/rotation, to support scene rotation
let outsideEntranceGF = new Vector4(23,0,37,7.243)
let entranceGF = new Vector4(28,1,32,7.243)
let girlGF = new Vector4(29.231,7.244,34.884,18.5)
let pathToStoneGF = new Vector4(28.8,18.6,34,24)
let reaperGF = new Vector4(14.465,26.931,20.494,31.085)
let dragonGF = new Vector4(40.227,26.35,48.38,31.562)

// Sprite destinations
let spriteEntranceLoc = new Vector3 (32, spriteHeight, 3.5)
let spriteGirlLoc  = new Vector3 (32, spriteHeight, 12)
let spritePathToStoneLoc  = new Vector3 (32, spriteHeight, 22)
let spritePathToReaperLoc  = new Vector3 (18, spriteHeight, 29)
let spritePathToDragonLoc = new Vector3 (40, spriteHeight, 29)
let spriteStoneLoc = new Vector3 (34, spriteHeight, 29)
let spriteFountainLoc  = new Vector3 (34, spriteHeight, 48)

// Timers
let girlTimeout = girlAnimationDuration * 2
let girlTimeoutTimer:number

let reaperTimeout = 2
let reaperTimeoutTimer:number

let dragonTimeout = 2
let dragonTimeoutTimer:number

function isPersonInGF(zone:Vector4){ // todo turn GFs into invisible planes, or triggers, with scene as root, or perform a one time rotation/translation on them based on scene's position/rotation, to support scene rotation
  let camera = Camera.instance
  //log("camera =", camera.position.x, camera.position.z)
  //log("zone =", zone)
  return ((camera.position.x >= zone.x)&&(camera.position.x <= zone.z)&&(camera.position.z >= zone.y)&&(camera.position.z <= zone.w))
}



const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  //position: new Vector3(16 * 4, 0, 0), // to make it have entrance on east instead of south
  rotation: new Quaternion(0, 0, 0, 1),
  //rotation: Quaternion.Euler(0,-90,0), // to make it have entrance on east instead of south
  scale: new Vector3(1, 1, 1)
})
scene.addComponentOrReplace(transform)
engine.addEntity(scene)

//////////////
// Animated Entities

// Four Trees
const tree1 = new Entity()
tree1.setParent(scene)
const gltfShape = new GLTFShape('models/Tree_Leafs_02/Tree_Leafs_02.glb')
tree1.addComponentOrReplace(gltfShape)
const transform_2 = new Transform({
  position: new Vector3(41.04726606433294, 3.552713678800501e-15, 39.15902312822593),
  rotation: new Quaternion(0, 0.4567794428455872, 0, 0.8895799798745895),
  scale: new Vector3(1, 1, 1)
})
tree1.addComponentOrReplace(transform_2)
engine.addEntity(tree1)

const tree2 = new Entity()
tree2.setParent(scene)
tree2.addComponentOrReplace(gltfShape)
const transform_3 = new Transform({
  position: new Vector3(23.714920185145132, 3.552713678800501e-15, 20.05414785006565),
  rotation: new Quaternion(0, 0.9755900075535573, 0, -0.2195999480001045),
  scale: new Vector3(1, 1, 1)
})
tree2.addComponentOrReplace(transform_3)
engine.addEntity(tree2)

const tree3 = new Entity()
tree3.setParent(scene)
tree3.addComponentOrReplace(gltfShape)
const transform_4 = new Transform({
  position: new Vector3(39.62738053671782, 2.1316282072803006e-14, 19.682027556621577),
  rotation: new Quaternion(0, 0.14050769886167888, 0, 0.9900795859730648),
  scale: new Vector3(1, 1, 1)
})
tree3.addComponentOrReplace(transform_4)
engine.addEntity(tree3)

const tree4 = new Entity()
tree4.setParent(scene)
tree4.addComponentOrReplace(gltfShape)
const transform_5 = new Transform({
  position: new Vector3(23.5, 0, 40),
  rotation: new Quaternion(0, -0.4395689874537646, 0, 0.8982088316582467),
  scale: new Vector3(1, 1, 1)
})
tree4.addComponentOrReplace(transform_5)
engine.addEntity(tree4)

// Animation of the above 4 trees
let tree_Leafs_02Animator = new Animator()
let tree_Leafs_02_2Animator = new Animator()
let tree_Leafs_02_3Animator = new Animator()
let tree_Leafs_02_4Animator = new Animator()

tree1.addComponent(tree_Leafs_02Animator)
tree2.addComponent(tree_Leafs_02_2Animator)
tree3.addComponent(tree_Leafs_02_3Animator)
tree4.addComponent(tree_Leafs_02_4Animator)

const clipLeafSwing = new AnimationState(treesAnimationName)
tree_Leafs_02Animator.addClip(clipLeafSwing)
tree_Leafs_02_2Animator.addClip(clipLeafSwing)
tree_Leafs_02_3Animator.addClip(clipLeafSwing)
tree_Leafs_02_4Animator.addClip(clipLeafSwing)

clipLeafSwing.looping = true
clipLeafSwing.play()


const tree1SoundfxClip = new AudioClip('soundfx/Tree1.mp3')
const tree1SoundSource = new AudioSource(tree1SoundfxClip)
tree1.addComponent(tree1SoundSource)
tree1SoundSource.playing = true
tree1SoundSource.loop = true
tree1SoundSource.volume = 1

const tree2SoundfxClip = new AudioClip('soundfx/Tree2.mp3')
const tree2SoundSource = new AudioSource(tree2SoundfxClip)
tree2.addComponent(tree2SoundSource)
tree2SoundSource.playing = true
tree2SoundSource.loop = true
tree2SoundSource.volume = 1

const tree3SoundfxClip = new AudioClip('soundfx/Tree3.mp3')
const tree3SoundSource = new AudioSource(tree3SoundfxClip)
tree3.addComponent(tree3SoundSource)
tree3SoundSource.playing = true
tree3SoundSource.loop = true
tree3SoundSource.volume = 1

const tree4SoundfxClip = new AudioClip('soundfx/Tree4.mp3')
const tree4SoundSource = new AudioSource(tree4SoundfxClip)
tree4.addComponent(tree4SoundSource)
tree4SoundSource.playing = true
tree4SoundSource.loop = true
tree4SoundSource.volume = 1

// Two girls
const girl_01 = new Entity()
girl_01.setParent(scene)
const gltfShape_11 = new GLTFShape('models/Girl/Girl.glb')
girl_01.addComponentOrReplace(gltfShape_11)
const transform_136 = new Transform({
  position: new Vector3(35.835650898970925, 1.7763568394002505e-15, 14.065292458693657),
  rotation: new Quaternion(0, -0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
girl_01.addComponentOrReplace(transform_136)
engine.addEntity(girl_01)

const girl_02 = new Entity()
girl_02.setParent(scene)
girl_02.addComponentOrReplace(gltfShape_11)
const transform_142 = new Transform({
  position: new Vector3(27.150909676209373, 0, 14.660903348901652),
  rotation: new Quaternion(0, 0.7068476501474317, 0, 0.7073658172975661),
  scale: new Vector3(1, 1, 1)
})
girl_02.addComponentOrReplace(transform_142)
engine.addEntity(girl_02)

let girl_01Animator = new Animator()
let girl_02Animator = new Animator()

girl_01.addComponent(girl_01Animator)
girl_02.addComponent(girl_02Animator)

const clipBow1 = new AnimationState(girlAnimationName)
const clipBow2 = new AnimationState(girlAnimationName)
clipBow1.looping = false
clipBow2.looping = false
girl_01Animator.addClip(clipBow1)
girl_02Animator.addClip(clipBow2)

const girlSoundfxClip = new AudioClip('soundfx/Girl.mp3')
const girlSoundSource = new AudioSource(girlSoundfxClip)
girl_01.addComponent(girlSoundSource)
girlSoundSource.playing = false
girlSoundSource.loop = false
girlSoundSource.volume = 0.5

const girl2SoundfxClip = new AudioClip('soundfx/Girl2.mp3')
const girl2SoundSource = new AudioSource(girl2SoundfxClip)
girl_02.addComponent(girl2SoundSource)
girl2SoundSource.playing = false
girl2SoundSource.loop = false
girl2SoundSource.volume = 0.5


// Floating stone
const floatingStone = new Entity()
floatingStone.setParent(scene)
const gltfShape_18 = new GLTFShape('models/FloatingStone/FloatingStone.glb')
floatingStone.addComponentOrReplace(gltfShape_18)
const transform_156 = new Transform({
position: new Vector3(32.002,0,28.912),
  rotation: new Quaternion(0, -0.380697137098344, 0, 0.9246997836082393),
  scale: new Vector3(1, 1, 1)
})

floatingStone.addComponentOrReplace(transform_156)
engine.addEntity(floatingStone)

let stoneAnimator = new Animator()
floatingStone.addComponent(stoneAnimator)
const clipFloat = new AnimationState(floatingStoneAnimationName)
stoneAnimator.addClip(clipFloat)
clipFloat.looping = false

const floatingStoneSoundfxClip = new AudioClip('soundfx/FloatingStone.mp3')
const floatingStoneSoundSource = new AudioSource(floatingStoneSoundfxClip)
floatingStone.addComponent(floatingStoneSoundSource)
floatingStoneSoundSource.playing = false
floatingStoneSoundSource.loop = false
floatingStoneSoundSource.volume = 1


// Grim Reaper
const reaper = new Entity()
reaper.setParent(scene)
const gltfShape_13 = new GLTFShape('models/GrimReaper/GrimReaper.glb')
reaper.addComponentOrReplace(gltfShape_13)
const transform_138 = new Transform({
  position: new Vector3(12.041252714166069, 2.6471916409275362, 29.290517916775944),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
reaper.addComponentOrReplace(transform_138)
engine.addEntity(reaper)

let reaperAnimator = new Animator()
reaper.addComponent(reaperAnimator)
const clipAttackSwing = new AnimationState(grimReaperAnimationName)
reaperAnimator.addClip(clipAttackSwing)
clipAttackSwing.looping = false

const reaperSoundfxClip = new AudioClip('soundfx/GrimReaper.mp3')
const reaperSoundSource = new AudioSource(reaperSoundfxClip)
reaper.addComponent(reaperSoundSource)
reaperSoundSource.playing = false
reaperSoundSource.loop = false
reaperSoundSource.volume = 1


// Dragon
const dragon = new Entity()
dragon.setParent(scene)
const gltfShape_14 = new GLTFShape('models/Dragon/Dragon.glb')  // was Statue_02
dragon.addComponentOrReplace(gltfShape_14)
const transform_141 = new Transform({
  position: new Vector3(50.073441346202884, 2.3165998060132367, 28.849713126724406),
  rotation: new Quaternion(0, -0.6991194291826842, 0, 0.7150049116889184),
  scale: new Vector3(1, 1, 1)
})
dragon.addComponentOrReplace(transform_141)
engine.addEntity(dragon)

let dragonAnimator = new Animator()
dragon.addComponent(dragonAnimator)
const clipAttack = new AnimationState(dragonAnimationName)
dragonAnimator.addClip(clipAttack)
clipAttack.looping = false

const dragonSoundfxClip = new AudioClip('soundfx/Dragon.mp3')
const dragonSoundSource = new AudioSource(dragonSoundfxClip)
dragon.addComponent(dragonSoundSource)
dragonSoundSource.playing = false
dragonSoundSource.loop = false
dragonSoundSource.volume = 1

/*
// Fog
const fog1 = new Entity()
fog1.setParent(scene)
const gltfShape_19 = new GLTFShape('models/fog.glb')
fog1.addComponentOrReplace(gltfShape_19)
const transform_177a = new Transform({
  position: new Vector3(32, 0.6244104203323912, 31.65677053215738),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(-25.543640665159447, -22.36389089961383, -25.232993638501224)
})
fog1.addComponentOrReplace(transform_177a)
engine.addEntity(fog1)

const fog2 = new Entity()
fog2.setParent(scene)
fog2.addComponentOrReplace(gltfShape_19)
const transform_177b = new Transform({
  position: new Vector3(32, 0.6244104203323912, 31.65677053215738),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(-23.543640665159447, -19.36389089961383, -23.232993638501224)
})
fog2.addComponentOrReplace(transform_177b)
engine.addEntity(fog2)

const fog3 = new Entity()
fog3.setParent(scene)
fog3.addComponentOrReplace(gltfShape_19)
const transform_177c = new Transform({
  position: new Vector3(32, 0.6244104203323912, 31.65677053215738),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(-21.543640665159447, -17.36389089961383, -21.232993638501224)
})
fog3.addComponentOrReplace(transform_177c)
engine.addEntity(fog3)
*/

// Fog
const fog1 = new Entity()
fog1.setParent(scene)
const gltfShape_19 = new GLTFShape('models/fog.glb')
fog1.addComponentOrReplace(gltfShape_19)
const transform_177a = new Transform({
  position: new Vector3(32, 0.6, 32),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(25, 22, 25)
})
fog1.addComponentOrReplace(transform_177a)
engine.addEntity(fog1)

const fog2 = new Entity()
fog2.setParent(scene)
fog2.addComponentOrReplace(gltfShape_19)
const transform_177b = new Transform({
  position: new Vector3(32, 0.6, 32),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(23, 19, 23)
})
fog2.addComponentOrReplace(transform_177b)
engine.addEntity(fog2)

const fog3 = new Entity()
fog3.setParent(scene)
fog3.addComponentOrReplace(gltfShape_19)
const transform_177c = new Transform({
  position: new Vector3(32, 0.6, 32),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(21, 17, 21)
})
fog3.addComponentOrReplace(transform_177c)
engine.addEntity(fog3)


/////////////////////////////// 
// Guidance sprite

const sprite = new Entity() // invisible location of sprite
const transformSprite = new Transform({
  position: spriteEntranceLoc,
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1,1,1)
})
sprite.addComponentOrReplace(transformSprite)
sprite.setParent(scene)

const spriteStoneShape = new GLTFShape('models/Glowing_Stone_01/Glowing_Stone_01.glb')
const spriteStone = new Entity
sprite.addComponentOrReplace(spriteStoneShape)
const transformSpriteStone = new Transform({
  position: new Vector3(0,0,0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1,1,1)
})
spriteStone.addComponentOrReplace(transformSpriteStone) 
spriteStone.setParent(sprite)

let spriteOrigin:Vector3 = spriteEntranceLoc
let spriteTarget:Vector3 = null

// Gong sounds (emanate from invisible dhildren of the Sprite)
let successSoundEntity = new Entity()
successSoundEntity.setParent(sprite)
let failureSoundEntity = new Entity()
failureSoundEntity.setParent(sprite)

let successAudioClip = new AudioClip('soundfx/Gong3.mp3')
let failureAudioClip = new AudioClip('soundfx/Gong4BendDown.mp3')
let successSoundSource = new AudioSource(successAudioClip)
let failureSoundSource = new AudioSource(failureAudioClip)

successSoundEntity.addComponent(new Transform({position:new Vector3(0,0,0)}))
successSoundEntity.addComponent(successSoundSource)
successSoundSource.playing = false
successSoundSource.loop = false
successSoundSource.volume = 0.3   

failureSoundEntity.addComponent(new Transform({position:new Vector3(0,0,0)}))
failureSoundEntity.addComponent(failureSoundSource)
failureSoundSource.playing = false
failureSoundSource.loop = false
failureSoundSource.volume = 0.7



engine.addEntity(sprite)

function moveSprite(destination:Vector3){
  // set up for the lerp based movement done in the TimerSystem
  spriteOrigin = sprite.getComponent(Transform).position
  spriteTarget = destination
  spriteMovementFraction = 0
  spriteMoving = true
}


///////////////////////////////////////////////////////////////////////
// Game State machine
/*
// Sprite destinations
let spriteEntranceLoc = new Vector3 (32, spriteHeight, 3.5)
let spriteGirlLoc  = new Vector3 (32, spriteHeight, 12)
let spritePathToStoneLoc  = new Vector3 (32, spriteHeight, 22)
let spritePathToReaperLoc  = new Vector3 (18, spriteHeight, 29)
let spritePathToDragonLoc = new Vector3 (40, spriteHeight, 29)
let spriteStoneLoc = new Vector3 (34, spriteHeight, 29)
let spriteFountainLoc  = new Vector3 (34, spriteHeight, 48)
*/
class GameState {
  state = 0 

  constructor(){
  }
    
  success(){
    this.state += 1
    log ("success, state now =",this.state)
    successSoundSource.playOnce()
    switch (this.state) {
      case 0: // has not entered entrance
        moveSprite(spriteEntranceLoc)
      break
      case 1: // has entered Entrance path
        moveSprite(spriteGirlLoc)
      break
      case 2: // has entered Girls zone
        moveSprite(spritePathToStoneLoc)
        girlTimeoutTimer = girlTimeout
        break
      case 3: // has moved beyond Girls zone soon enough
        moveSprite(spritePathToReaperLoc)
        reaperTimeoutTimer = reaperTimeout
        break
      case 4: // has triggered Reaper but moved away soon enough
        moveSprite(spritePathToDragonLoc)
        dragonTimeoutTimer = dragonTimeout
        break
      case 5: // has triggered Dragon but moved away soon enough
        moveSprite(spriteStoneLoc)
        break
      case 6: // has triggered stone
        moveSprite(spriteFountainLoc)
        break
      case 7: // has made a wish from state 6: get a reward
        //moveSprite(spriteEntranceLoc) //!CF what to do with sprite here?  Change its appearance?
        break
      default:
        break
    }
  }

  failure(){
    this.state = 0
    log ("failure, state now =",this.state)
    failureSoundSource.playOnce()
    moveSprite(spriteEntranceLoc)
    stoneClicked = false
  }

  /*
  let outsideEntranceGF = new Vector4(23,0,37,7.243)
let entranceGF = new Vector4(28,1,32,7.243)
let girlGF = new Vector4(29.231,7.244,34.884,18.5)
let pathToStoneGF = new Vector4(28.8,18.6,34,24)
let reaperGF = new Vector4(14.465,26.931,20.494,31.085)
let dragonGF = new Vector4(40.227,26.35,48.38,31.562)
*/
  checkState(dt:number){
    switch (this.state) {
      case 0: // was outside entrance
        if (isPersonInGF(entranceGF)){
          this.success()
        }
        break
      case 1: // was in entrance path
        if (isPersonInGF(girlGF)){
          this.success()
          //!CF todo initiate a timer to limit time in Girls zone
        }
        break
      case 2: // had entered in Girl zone
        girlTimeoutTimer -= dt
        if (girlTimeoutTimer <= 0){
          this.failure()
        }
        else if (!isPersonInGF(outsideEntranceGF)&&!isPersonInGF(entranceGF)&&!isPersonInGF(girlGF)){
          this.success()
        }
        break
      case 3: // approaching or entering Reaper zone
        if (isPersonInGF(reaperGF)) {
          reaperTimeoutTimer -= dt
          if (reaperTimeoutTimer <=0){
            //!CF todo: push player back with an invisible collider
            this.failure()
          }
        }
        else if ((reaperTimeoutTimer < reaperTimeout)&&(reaperTimeout>0)){
          // countdown had started but not finished because they stepped out soon after starting it
          this.success()
        }
        break
      case 4: // approaching or entering Dragon zone
          if (isPersonInGF(dragonGF)) {
            dragonTimeoutTimer -= dt
            if (dragonTimeoutTimer <=0){
              //!CF todo: flames and smoke around the avatar
              this.failure()
            }
          }
          else if ((dragonTimeoutTimer < dragonTimeout)&&(dragonTimeout>0)){
            // countdown had started but not finished because they stepped out soon after starting it
            this.success()
          }
          break
      case 5: // ok to click stone
        // state is driven forward by the event handler's if state == 5 success, else failure
        break
      case 6: // Stone clicked at the right time. Making a wish from this state will create a reward
        break
      case 7: // made a wish
        // !CF todo provide reward
        break
      default:
        break
    }

  }
}

let gS = new GameState()

// floading stone click handler, must come after GameState
floatingStone.addComponent(new OnClick(()=>{
  if (floatingStoneTimer <=0 ) {
    stoneClicked = true
    //initiate animation, let it run until done
  	// stop previous animation to be sure it starts from beginning
    clipFloat.stop()
    // play animation
    clipFloat.play()//
    floatingStoneSoundSource.playOnce()
    floatingStoneTimer = floatingStoneAnimationDuration // assuming speed = 1
  }
  if (gS.state != 5) {
    gS.failure()
  }
}))


////////////////////////////////////////////////////////////////////////
// System for timers, geofence testing, and the triggering of animations
class TimerSystem implements ISystem {

  // Fog control
  time = 0

  fog1Speed = 0.15
  fog2Speed = 0.19
  fog3Speed = 0.05

  fog1Amplitude = .0008
  fog2Amplitude = .001
  fog3Amplitude = .002

  update(dt:number){
 
    this.time += dt
    gS.checkState(dt)

    // Sprite movement as needed

    if (spriteMoving)  {
      if (spriteMovementFraction<1) {
        let transform = sprite.getComponent(Transform)
        transform.position = Vector3.Lerp(
          spriteOrigin,
          spriteTarget,
          spriteMovementFraction
        )
        spriteMovementFraction += dt * spriteSpeed
      }
      else {
        spriteMoving = false
        spriteMovementFraction = 0
      }
    }


    // Fog Control
    let fogTransform:Transform = null

    fogTransform = fog1.getComponent(Transform)
    fogTransform.rotate(Vector3.Up(), this.fog1Amplitude * Math.cos(this.time*this.fog1Speed) / this.fog1Speed)

    fogTransform = fog2.getComponent(Transform)
    fogTransform.rotate(Vector3.Up(), this.fog2Amplitude * Math.cos(1+this.time*this.fog2Speed) / this.fog2Speed)

    fogTransform = fog3.getComponent(Transform)
    fogTransform.rotate(Vector3.Up(), this.fog3Amplitude * Math.cos(2+this.time*this.fog3Speed) / this.fog2Speed)
    
    let pos = sprite.getComponent(Transform).position
    pos.y = spriteHeight + Math.sin(this.time*spriteHeightSpeed)*spriteHeightAmplitude

    // Animated objectscontrol
    if ( girlTimer > 0) {
      girlTimer -= dt
    }
    else if (isPersonInGF(girlGF)) {
      clipBow1.stop()
      clipBow2.stop()
      clipBow1.play()
      clipBow2.play()
      girlSoundSource.playOnce()
      girl2SoundSource.playOnce()
      girlTimer = girlAnimationDuration
    }    

    if ( grimReaperTimer > 0) {
      grimReaperTimer -= dt
    } 
    else if (isPersonInGF(reaperGF)) {
      clipAttackSwing.stop()
      //reaperSoundSource.playing = false
      clipAttackSwing.play()
      reaperSoundSource.playOnce()
      grimReaperTimer = grimReaperAnimationDuration
    }

    if ( dragonTimer > 0) {
      dragonTimer -= dt
    }
    else if (isPersonInGF(dragonGF)) {
      clipAttack.stop()
      clipAttack.play()
      dragonSoundSource.playOnce()
      dragonTimer = dragonAnimationDuration
    }

    if (floatingStoneTimer > 0 ) {
      floatingStoneTimer -= dt
      if ((gS.state == 5) && (floatingStoneTimer <= dt)){ //!CF
        gS.success()
      }
    }

  }
}

let timerSystem = new TimerSystem()
engine.addSystem(timerSystem)


///////////////////////
// Non animated entities

const floorBaseGrass_01 = new Entity()
floorBaseGrass_01.setParent(scene)
const gltfShape_2 = new GLTFShape('models/FloorBaseGrass_01/FloorBaseGrass_01.glb')
floorBaseGrass_01.addComponentOrReplace(gltfShape_2)
const transform_6 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01.addComponentOrReplace(transform_6)
engine.addEntity(floorBaseGrass_01)

const floorBaseGrass_01_2 = new Entity()
floorBaseGrass_01_2.setParent(scene)
floorBaseGrass_01_2.addComponentOrReplace(gltfShape_2)
const transform_7 = new Transform({
  position: new Vector3(24, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01_2.addComponentOrReplace(transform_7)
engine.addEntity(floorBaseGrass_01_2)

const floorBaseGrass_01_3 = new Entity()
floorBaseGrass_01_3.setParent(scene)
floorBaseGrass_01_3.addComponentOrReplace(gltfShape_2)
const transform_8 = new Transform({
  position: new Vector3(40, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01_3.addComponentOrReplace(transform_8)
engine.addEntity(floorBaseGrass_01_3)

const floorBaseGrass_01_4 = new Entity()
floorBaseGrass_01_4.setParent(scene)
floorBaseGrass_01_4.addComponentOrReplace(gltfShape_2)
const transform_9 = new Transform({
  position: new Vector3(56, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01_4.addComponentOrReplace(transform_9)
engine.addEntity(floorBaseGrass_01_4)

const floorBaseGrass_01_5 = new Entity()
floorBaseGrass_01_5.setParent(scene)
floorBaseGrass_01_5.addComponentOrReplace(gltfShape_2)
const transform_10 = new Transform({
  position: new Vector3(8, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01_5.addComponentOrReplace(transform_10)
engine.addEntity(floorBaseGrass_01_5)

const floorBaseGrass_01_6 = new Entity()
floorBaseGrass_01_6.setParent(scene)
floorBaseGrass_01_6.addComponentOrReplace(gltfShape_2)
const transform_11 = new Transform({
  position: new Vector3(24, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01_6.addComponentOrReplace(transform_11)
engine.addEntity(floorBaseGrass_01_6)

const floorBaseGrass_01_7 = new Entity()
floorBaseGrass_01_7.setParent(scene)
floorBaseGrass_01_7.addComponentOrReplace(gltfShape_2)
const transform_12 = new Transform({
  position: new Vector3(40, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01_7.addComponentOrReplace(transform_12)
engine.addEntity(floorBaseGrass_01_7)

const floorBaseGrass_01_8 = new Entity()
floorBaseGrass_01_8.setParent(scene)
floorBaseGrass_01_8.addComponentOrReplace(gltfShape_2)
const transform_13 = new Transform({
  position: new Vector3(56, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01_8.addComponentOrReplace(transform_13)
engine.addEntity(floorBaseGrass_01_8)

const floorBaseGrass_01_9 = new Entity()
floorBaseGrass_01_9.setParent(scene)
floorBaseGrass_01_9.addComponentOrReplace(gltfShape_2)
const transform_14 = new Transform({
  position: new Vector3(8, 0, 40),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01_9.addComponentOrReplace(transform_14)
engine.addEntity(floorBaseGrass_01_9)

const floorBaseGrass_01_10 = new Entity()
floorBaseGrass_01_10.setParent(scene)
floorBaseGrass_01_10.addComponentOrReplace(gltfShape_2)
const transform_15 = new Transform({
  position: new Vector3(24, 0, 40),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01_10.addComponentOrReplace(transform_15)
engine.addEntity(floorBaseGrass_01_10)

const floorBaseGrass_01_11 = new Entity()
floorBaseGrass_01_11.setParent(scene)
floorBaseGrass_01_11.addComponentOrReplace(gltfShape_2)
const transform_16 = new Transform({
  position: new Vector3(40, 0, 40),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01_11.addComponentOrReplace(transform_16)
engine.addEntity(floorBaseGrass_01_11)

const floorBaseGrass_01_12 = new Entity()
floorBaseGrass_01_12.setParent(scene)
floorBaseGrass_01_12.addComponentOrReplace(gltfShape_2)
const transform_17 = new Transform({
  position: new Vector3(56, 0, 40),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01_12.addComponentOrReplace(transform_17)
engine.addEntity(floorBaseGrass_01_12)

const floorBaseGrass_01_13 = new Entity()
floorBaseGrass_01_13.setParent(scene)
floorBaseGrass_01_13.addComponentOrReplace(gltfShape_2)
const transform_18 = new Transform({
  position: new Vector3(8, 0, 56),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01_13.addComponentOrReplace(transform_18)
engine.addEntity(floorBaseGrass_01_13)

const floorBaseGrass_01_14 = new Entity()
floorBaseGrass_01_14.setParent(scene)
floorBaseGrass_01_14.addComponentOrReplace(gltfShape_2)
const transform_19 = new Transform({
  position: new Vector3(24, 0, 56),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01_14.addComponentOrReplace(transform_19)
engine.addEntity(floorBaseGrass_01_14)

const floorBaseGrass_01_15 = new Entity()
floorBaseGrass_01_15.setParent(scene)
floorBaseGrass_01_15.addComponentOrReplace(gltfShape_2)
const transform_20 = new Transform({
  position: new Vector3(40, 0, 56),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01_15.addComponentOrReplace(transform_20)
engine.addEntity(floorBaseGrass_01_15)

const floorBaseGrass_01_16 = new Entity()
floorBaseGrass_01_16.setParent(scene)
floorBaseGrass_01_16.addComponentOrReplace(gltfShape_2)
const transform_21 = new Transform({
  position: new Vector3(56, 0, 56),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01_16.addComponentOrReplace(transform_21)
engine.addEntity(floorBaseGrass_01_16)

const tree_Dead_03 = new Entity()
tree_Dead_03.setParent(scene)
const gltfShape_3 = new GLTFShape('models/Tree_Dead_03/Tree_Dead_03.glb')
tree_Dead_03.addComponentOrReplace(gltfShape_3)
const transform_22 = new Transform({
  position: new Vector3(29.20603082918675, 3.552713678800501e-15, 7.7939606244533834),
  rotation: new Quaternion(0, 0.9245960535811861, 0, 0.3809489961951574),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03.addComponentOrReplace(transform_22)
engine.addEntity(tree_Dead_03)

const tree_Dead_03_2 = new Entity()
tree_Dead_03_2.setParent(scene)
tree_Dead_03_2.addComponentOrReplace(gltfShape_3)
const transform_23 = new Transform({
  position: new Vector3(32.969513442665686, 7.105427357601002e-15, 7.437568622543614),
  rotation: new Quaternion(0, -0.4597514347138579, 0, 0.8880476441489779),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_2.addComponentOrReplace(transform_23)
engine.addEntity(tree_Dead_03_2)

const tree_Dead_01 = new Entity()
tree_Dead_01.setParent(scene)
const gltfShape_4 = new GLTFShape('models/Tree_Dead_01/Tree_Dead_01.glb')
tree_Dead_01.addComponentOrReplace(gltfShape_4)
const transform_24 = new Transform({
  position: new Vector3(35.63083265414664, 3.552713678800501e-15, 9.40452535191763),
  rotation: new Quaternion(0, 0.6666455263831614, 0, 0.7453749004047011),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01.addComponentOrReplace(transform_24)
engine.addEntity(tree_Dead_01)

const tree_Dead_01_2 = new Entity()
tree_Dead_01_2.setParent(scene)
tree_Dead_01_2.addComponentOrReplace(gltfShape_4)
const transform_25 = new Transform({
  position: new Vector3(26.203456072672775, 1.5987211554602254e-14, 8.875850248903761),
  rotation: new Quaternion(0, -0.246449868339277, 0, 0.9691555408682105),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_2.addComponentOrReplace(transform_25)
engine.addEntity(tree_Dead_01_2)

const tree_Dead_02 = new Entity()
tree_Dead_02.setParent(scene)
const gltfShape_5 = new GLTFShape('models/Tree_Dead_02/Tree_Dead_02.glb')
tree_Dead_02.addComponentOrReplace(gltfShape_5)
const transform_26 = new Transform({
  position: new Vector3(23.757155850585846, 0, 12.099260760434953),
  rotation: new Quaternion(0, -0.9017339147093296, 0, 0.43229150704471153),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02.addComponentOrReplace(transform_26)
engine.addEntity(tree_Dead_02)

const tree_Dead_02_2 = new Entity()
tree_Dead_02_2.setParent(scene)
tree_Dead_02_2.addComponentOrReplace(gltfShape_5)
const transform_27 = new Transform({
  position: new Vector3(38.86622767218073, 0, 11.35035451965894),
  rotation: new Quaternion(0, -0.16560606568347966, 0, 0.9861919848634135),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_2.addComponentOrReplace(transform_27)
engine.addEntity(tree_Dead_02_2)

const tree_Dead_01_3 = new Entity()
tree_Dead_01_3.setParent(scene)
tree_Dead_01_3.addComponentOrReplace(gltfShape_4)
const transform_28 = new Transform({
  position: new Vector3(42.5, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_3.addComponentOrReplace(transform_28)
engine.addEntity(tree_Dead_01_3)

const tree_Dead_02_3 = new Entity()
tree_Dead_02_3.setParent(scene)
tree_Dead_02_3.addComponentOrReplace(gltfShape_5)
const transform_29 = new Transform({
  position: new Vector3(45.90098362046045, 0, 10.682333943255546),
  rotation: new Quaternion(0, -0.9666873573117811, 0, 0.25596006175488606),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_3.addComponentOrReplace(transform_29)
engine.addEntity(tree_Dead_02_3)

const tree_Dead_03_3 = new Entity()
tree_Dead_03_3.setParent(scene)
tree_Dead_03_3.addComponentOrReplace(gltfShape_3)
const transform_30 = new Transform({
  position: new Vector3(38, 0, 5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_3.addComponentOrReplace(transform_30)
engine.addEntity(tree_Dead_03_3)

const tree_Dead_01_4 = new Entity()
tree_Dead_01_4.setParent(scene)
tree_Dead_01_4.addComponentOrReplace(gltfShape_4)
const transform_31 = new Transform({
  position: new Vector3(41.68152990461617, 7.105427357601002e-15, 8.315998460974942),
  rotation: new Quaternion(0, 0.9916068952230152, 0, 0.1292894633996589),
  scale: new Vector3(1, 1.1843683154325184, 1)
})
tree_Dead_01_4.addComponentOrReplace(transform_31)
engine.addEntity(tree_Dead_01_4)

const tree_Dead_01_5 = new Entity()
tree_Dead_01_5.setParent(scene)
tree_Dead_01_5.addComponentOrReplace(gltfShape_4)
const transform_32 = new Transform({
  position: new Vector3(46.54241333182927, 7.105427357601002e-15, 15.674573850064775),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_5.addComponentOrReplace(transform_32)
engine.addEntity(tree_Dead_01_5)

const tree_Dead_01_6 = new Entity()
tree_Dead_01_6.setParent(scene)
tree_Dead_01_6.addComponentOrReplace(gltfShape_4)
const transform_33 = new Transform({
  position: new Vector3(49.85670243649861, 0, 13.32355835137065),
  rotation: new Quaternion(0, 0.7500250197084415, 0, 0.661409457001752),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_6.addComponentOrReplace(transform_33)
engine.addEntity(tree_Dead_01_6)

const tree_Dead_01_7 = new Entity()
tree_Dead_01_7.setParent(scene)
tree_Dead_01_7.addComponentOrReplace(gltfShape_4)
const transform_34 = new Transform({
  position: new Vector3(21.90223696069407, 0, 7.486686784960985),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_7.addComponentOrReplace(transform_34)
engine.addEntity(tree_Dead_01_7)

const tree_Dead_03_4 = new Entity()
tree_Dead_03_4.setParent(scene)
tree_Dead_03_4.addComponentOrReplace(gltfShape_3)
const transform_35 = new Transform({
  position: new Vector3(22.710888946080736, 1.4210854715202004e-14, 3.156184993274024),
  rotation: new Quaternion(0, 0.622684895550544, 0, 0.7824726965544597),
  scale: new Vector3(1, 1.205286632100122, 1)
})
tree_Dead_03_4.addComponentOrReplace(transform_35)
engine.addEntity(tree_Dead_03_4)

const tree_Dead_01_8 = new Entity()
tree_Dead_01_8.setParent(scene)
tree_Dead_01_8.addComponentOrReplace(gltfShape_4)
const transform_36 = new Transform({
  position: new Vector3(20.1019643173934, 0, 13.278055512447624),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_8.addComponentOrReplace(transform_36)
engine.addEntity(tree_Dead_01_8)

const tree_Dead_02_4 = new Entity()
tree_Dead_02_4.setParent(scene)
tree_Dead_02_4.addComponentOrReplace(gltfShape_5)
const transform_37 = new Transform({
  position: new Vector3(17.208282013884535, 1.4210854715202004e-14, 8.905166245929031),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 0.9557239349265689, 1)
})
tree_Dead_02_4.addComponentOrReplace(transform_37)
engine.addEntity(tree_Dead_02_4)

const tree_Dead_03_5 = new Entity()
tree_Dead_03_5.setParent(scene)
tree_Dead_03_5.addComponentOrReplace(gltfShape_3)
const transform_38 = new Transform({
  position: new Vector3(17.085604503508566, 0, 17.091032712612922),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_5.addComponentOrReplace(transform_38)
engine.addEntity(tree_Dead_03_5)

const tree_Dead_01_9 = new Entity()
tree_Dead_01_9.setParent(scene)
tree_Dead_01_9.addComponentOrReplace(gltfShape_4)
const transform_39 = new Transform({
  position: new Vector3(14.819149700031705, 7.105427357601002e-15, 13.613356862288681),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_9.addComponentOrReplace(transform_39)
engine.addEntity(tree_Dead_01_9)

const tree_Dead_01_10 = new Entity()
tree_Dead_01_10.setParent(scene)
tree_Dead_01_10.addComponentOrReplace(gltfShape_4)
const transform_40 = new Transform({
  position: new Vector3(12.643879349667575, 0, 22.242994074512914),
  rotation: new Quaternion(0, 0.3959850193109211, 0, 0.9182569708318742),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_10.addComponentOrReplace(transform_40)
engine.addEntity(tree_Dead_01_10)

const tree_Dead_03_6 = new Entity()
tree_Dead_03_6.setParent(scene)
tree_Dead_03_6.addComponentOrReplace(gltfShape_3)
const transform_41 = new Transform({
  position: new Vector3(10.087194168010287, 0, 16.841738168292572),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 0.8977594980569581, 1)
})
tree_Dead_03_6.addComponentOrReplace(transform_41)
engine.addEntity(tree_Dead_03_6)

const tree_Dead_02_5 = new Entity()
tree_Dead_02_5.setParent(scene)
tree_Dead_02_5.addComponentOrReplace(gltfShape_5)
const transform_42 = new Transform({
  position: new Vector3(14.28254857785641, 0, 18.72389402686853),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_5.addComponentOrReplace(transform_42)
engine.addEntity(tree_Dead_02_5)

const tree_Dead_03_7 = new Entity()
tree_Dead_03_7.setParent(scene)
tree_Dead_03_7.addComponentOrReplace(gltfShape_3)
const transform_43 = new Transform({
  position: new Vector3(49.212802774269264, 0, 19.167321570057247),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_7.addComponentOrReplace(transform_43)
engine.addEntity(tree_Dead_03_7)

const tree_Dead_02_6 = new Entity()
tree_Dead_02_6.setParent(scene)
tree_Dead_02_6.addComponentOrReplace(gltfShape_5)
const transform_44 = new Transform({
  position: new Vector3(50.817772961452214, 0, 22.517235153082414),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_6.addComponentOrReplace(transform_44)
engine.addEntity(tree_Dead_02_6)

const tree_Dead_03_8 = new Entity()
tree_Dead_03_8.setParent(scene)
tree_Dead_03_8.addComponentOrReplace(gltfShape_3)
const transform_45 = new Transform({
  position: new Vector3(10.471649544994282, 0, 26.00333500896975),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_8.addComponentOrReplace(transform_45)
engine.addEntity(tree_Dead_03_8)

const tree_Dead_02_7 = new Entity()
tree_Dead_02_7.setParent(scene)
tree_Dead_02_7.addComponentOrReplace(gltfShape_5)
const transform_46 = new Transform({
  position: new Vector3(12.623575061910053, 0, 34.270560417519626),
  rotation: new Quaternion(0, 0.9471829471203, 0, 0.320693724111501),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_7.addComponentOrReplace(transform_46)
engine.addEntity(tree_Dead_02_7)

const tree_Dead_01_11 = new Entity()
tree_Dead_01_11.setParent(scene)
tree_Dead_01_11.addComponentOrReplace(gltfShape_4)
const transform_47 = new Transform({
  position: new Vector3(50.914605889973764, 1.7763568394002505e-15, 34.09869779601859),
  rotation: new Quaternion(0, 0.44071064722721726, 0, 0.8976492218124895),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_11.addComponentOrReplace(transform_47)
engine.addEntity(tree_Dead_01_11)

const tree_Dead_03_9 = new Entity()
tree_Dead_03_9.setParent(scene)
tree_Dead_03_9.addComponentOrReplace(gltfShape_3)
const transform_48 = new Transform({
  position: new Vector3(52.99288018653094, 0, 26.354272957082685),
  rotation: new Quaternion(0, 0.38199797611270425, 0, 0.9241631599700338),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_9.addComponentOrReplace(transform_48)
engine.addEntity(tree_Dead_03_9)

const tree_Dead_01_12 = new Entity()
tree_Dead_01_12.setParent(scene)
tree_Dead_01_12.addComponentOrReplace(gltfShape_4)
const transform_49 = new Transform({
  position: new Vector3(50.43653861468261, 0, 39.920496821028344),
  rotation: new Quaternion(0, 0.9998781993962087, 0, 0.015607253832574088),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_12.addComponentOrReplace(transform_49)
engine.addEntity(tree_Dead_01_12)

const tree_Dead_02_8 = new Entity()
tree_Dead_02_8.setParent(scene)
tree_Dead_02_8.addComponentOrReplace(gltfShape_5)
const transform_50 = new Transform({
  position: new Vector3(48.93077141384097, 7.105427357601002e-15, 43.084771881865954),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_8.addComponentOrReplace(transform_50)
engine.addEntity(tree_Dead_02_8)

const tree_Dead_01_13 = new Entity()
tree_Dead_01_13.setParent(scene)
tree_Dead_01_13.addComponentOrReplace(gltfShape_4)
const transform_51 = new Transform({
  position: new Vector3(13.186889097823205, 7.105427357601002e-15, 38.15983889368907),
  rotation: new Quaternion(0, -0.3830886922855635, 0, 0.9237115642033162),
  scale: new Vector3(1, 1.1486172034218471, 1)
})
tree_Dead_01_13.addComponentOrReplace(transform_51)
engine.addEntity(tree_Dead_01_13)

const tree_Dead_03_10 = new Entity()
tree_Dead_03_10.setParent(scene)
tree_Dead_03_10.addComponentOrReplace(gltfShape_3)
const transform_52 = new Transform({
  position: new Vector3(14.409182275613757, 0, 42.23999102854328),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_10.addComponentOrReplace(transform_52)
engine.addEntity(tree_Dead_03_10)

const tree_Dead_02_9 = new Entity()
tree_Dead_02_9.setParent(scene)
tree_Dead_02_9.addComponentOrReplace(gltfShape_5)
const transform_53 = new Transform({
  position: new Vector3(16.944078587591477, 0, 44.72656676159725),
  rotation: new Quaternion(0, 0.4235412081714341, 0, 0.9058768376444349),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_9.addComponentOrReplace(transform_53)
engine.addEntity(tree_Dead_02_9)

const tree_Dead_01_14 = new Entity()
tree_Dead_01_14.setParent(scene)
tree_Dead_01_14.addComponentOrReplace(gltfShape_4)
const transform_54 = new Transform({
  position: new Vector3(19.13761314116579, 0, 46.98074504756663),
  rotation: new Quaternion(0, -0.2268499096003928, 0, 0.9739297297620061),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_14.addComponentOrReplace(transform_54)
engine.addEntity(tree_Dead_01_14)

const tree_Dead_01_15 = new Entity()
tree_Dead_01_15.setParent(scene)
tree_Dead_01_15.addComponentOrReplace(gltfShape_4)
const transform_55 = new Transform({
  position: new Vector3(45.800256666290394, 2.842170943040401e-14, 45.925717950991455),
  rotation: new Quaternion(0, 0.33491487258398905, 0, 0.9422483898219463),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_15.addComponentOrReplace(transform_55)
engine.addEntity(tree_Dead_01_15)

const tree_Dead_02_10 = new Entity()
tree_Dead_02_10.setParent(scene)
tree_Dead_02_10.addComponentOrReplace(gltfShape_5)
const transform_56 = new Transform({
  position: new Vector3(42.76703464290109, 0, 48.59200853116191),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_10.addComponentOrReplace(transform_56)
engine.addEntity(tree_Dead_02_10)

const tree_Dead_01_16 = new Entity()
tree_Dead_01_16.setParent(scene)
tree_Dead_01_16.addComponentOrReplace(gltfShape_4)
const transform_57 = new Transform({
  position: new Vector3(38.516736686408436, 2.1316282072803006e-14, 53.0754421365029),
  rotation: new Quaternion(0, -0.9840843394269926, 0, 0.17770203401913914),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_16.addComponentOrReplace(transform_57)
engine.addEntity(tree_Dead_01_16)

const tree_Dead_03_11 = new Entity()
tree_Dead_03_11.setParent(scene)
tree_Dead_03_11.addComponentOrReplace(gltfShape_3)
const transform_58 = new Transform({
  position: new Vector3(22.34933042202838, 7.105427357601002e-15, 50.494055462338224),
  rotation: new Quaternion(0, 0.33542386576860933, 0, 0.9420673172724133),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_11.addComponentOrReplace(transform_58)
engine.addEntity(tree_Dead_03_11)

const tree_Dead_03_12 = new Entity()
tree_Dead_03_12.setParent(scene)
tree_Dead_03_12.addComponentOrReplace(gltfShape_3)
const transform_59 = new Transform({
  position: new Vector3(31.99442644316879, 2.4868995751603507e-14, 53.23954066540032),
  rotation: new Quaternion(0, 0.786742132601884, 0, 0.6172817969040072),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_12.addComponentOrReplace(transform_59)
engine.addEntity(tree_Dead_03_12)

const tree_Dead_01_17 = new Entity()
tree_Dead_01_17.setParent(scene)
tree_Dead_01_17.addComponentOrReplace(gltfShape_4)
const transform_60 = new Transform({
  position: new Vector3(38.59554713107279, 0, 48.32457337685789),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_17.addComponentOrReplace(transform_60)
engine.addEntity(tree_Dead_01_17)

const tree_Dead_02_11 = new Entity()
tree_Dead_02_11.setParent(scene)
tree_Dead_02_11.addComponentOrReplace(gltfShape_5)
const transform_61 = new Transform({
  position: new Vector3(26.33876971633022, 0, 49.815750646637994),
  rotation: new Quaternion(0, 0.302883507857151, 0, 0.9530275865200057),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_11.addComponentOrReplace(transform_61)
engine.addEntity(tree_Dead_02_11)

const tree_Dead_01_18 = new Entity()
tree_Dead_01_18.setParent(scene)
tree_Dead_01_18.addComponentOrReplace(gltfShape_4)
const transform_62 = new Transform({
  position: new Vector3(7.011220038820353, 0, 26.01181290865041),
  rotation: new Quaternion(0, 0.34599542239256736, 0, 0.9382362003692827),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_18.addComponentOrReplace(transform_62)
engine.addEntity(tree_Dead_01_18)

const tree_Dead_02_12 = new Entity()
tree_Dead_02_12.setParent(scene)
tree_Dead_02_12.addComponentOrReplace(gltfShape_5)
const transform_63 = new Transform({
  position: new Vector3(8, 0, 21.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1.1994081801160732, 1)
})
tree_Dead_02_12.addComponentOrReplace(transform_63)
engine.addEntity(tree_Dead_02_12)

const tree_Dead_03_13 = new Entity()
tree_Dead_03_13.setParent(scene)
tree_Dead_03_13.addComponentOrReplace(gltfShape_3)
const transform_64 = new Transform({
  position: new Vector3(7.417847979181545, 0, 31.491732618375455),
  rotation: new Quaternion(0, 0.7089384664782553, 0, 0.7052703387690847),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_13.addComponentOrReplace(transform_64)
engine.addEntity(tree_Dead_03_13)

const tree_Dead_01_19 = new Entity()
tree_Dead_01_19.setParent(scene)
tree_Dead_01_19.addComponentOrReplace(gltfShape_4)
const transform_65 = new Transform({
  position: new Vector3(9, 0, 35.5),
  rotation: new Quaternion(0, -0.2681210196318268, 0, 0.963385239056313),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_19.addComponentOrReplace(transform_65)
engine.addEntity(tree_Dead_01_19)

const tree_Dead_01_20 = new Entity()
tree_Dead_01_20.setParent(scene)
tree_Dead_01_20.addComponentOrReplace(gltfShape_4)
const transform_66 = new Transform({
  position: new Vector3(11.91070290457928, 0, 46.665446905167215),
  rotation: new Quaternion(0, 0.8267159471863407, 0, -0.5626195363367615),
  scale: new Vector3(1, 0.9516289799653315, 1)
})
tree_Dead_01_20.addComponentOrReplace(transform_66)
engine.addEntity(tree_Dead_01_20)

const tree_Dead_02_13 = new Entity()
tree_Dead_02_13.setParent(scene)
tree_Dead_02_13.addComponentOrReplace(gltfShape_5)
const transform_67 = new Transform({
  position: new Vector3(9.5, 0, 41.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1.244396988005807, 1)
})
tree_Dead_02_13.addComponentOrReplace(transform_67)
engine.addEntity(tree_Dead_02_13)

const tree_Dead_01_21 = new Entity()
tree_Dead_01_21.setParent(scene)
tree_Dead_01_21.addComponentOrReplace(gltfShape_4)
const transform_68 = new Transform({
  position: new Vector3(6.9060489157950755, 0, 45.81490825606855),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_21.addComponentOrReplace(transform_68)
engine.addEntity(tree_Dead_01_21)

const tree_Dead_03_14 = new Entity()
tree_Dead_03_14.setParent(scene)
tree_Dead_03_14.addComponentOrReplace(gltfShape_3)
const transform_69 = new Transform({
  position: new Vector3(4.5, 0, 39),
  rotation: new Quaternion(0, 0, -0.00962376712697746, 0.999953690480857),
  scale: new Vector3(1, 1.0303209999122247, 1)
})
tree_Dead_03_14.addComponentOrReplace(transform_69)
engine.addEntity(tree_Dead_03_14)

const tree_Dead_02_14 = new Entity()
tree_Dead_02_14.setParent(scene)
tree_Dead_02_14.addComponentOrReplace(gltfShape_5)
const transform_70 = new Transform({
  position: new Vector3(4.091565201053442, 0.0005853236148851693, 34.26394447719222),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_14.addComponentOrReplace(transform_70)
engine.addEntity(tree_Dead_02_14)

const tree_Dead_03_15 = new Entity()
tree_Dead_03_15.setParent(scene)
tree_Dead_03_15.addComponentOrReplace(gltfShape_3)
const transform_71 = new Transform({
  position: new Vector3(3.271907542311114, 7.105427357601002e-15, 29.35530769879855),
  rotation: new Quaternion(0, -0.4344680615061212, 0, 0.9006872395738231),
  scale: new Vector3(1, 1.1604138739912937, 1)
})
tree_Dead_03_15.addComponentOrReplace(transform_71)
engine.addEntity(tree_Dead_03_15)

const tree_Dead_03_16 = new Entity()
tree_Dead_03_16.setParent(scene)
tree_Dead_03_16.addComponentOrReplace(gltfShape_3)
const transform_72 = new Transform({
  position: new Vector3(16.34818401761664, 7.105427357601002e-15, 51.46607494517039),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_16.addComponentOrReplace(transform_72)
engine.addEntity(tree_Dead_03_16)

const tree_Dead_02_15 = new Entity()
tree_Dead_02_15.setParent(scene)
tree_Dead_02_15.addComponentOrReplace(gltfShape_5)
const transform_73 = new Transform({
  position: new Vector3(20.35703918818136, 0, 53.1578343621669),
  rotation: new Quaternion(0, 0.9583451017013018, 0, 0.28561279040883547),
  scale: new Vector3(1, 1.0838332747234674, 1)
})
tree_Dead_02_15.addComponentOrReplace(transform_73)
engine.addEntity(tree_Dead_02_15)

const tree_Dead_03_17 = new Entity()
tree_Dead_03_17.setParent(scene)
tree_Dead_03_17.addComponentOrReplace(gltfShape_3)
const transform_74 = new Transform({
  position: new Vector3(26.408926359720194, 0, 54.38282638560474),
  rotation: new Quaternion(0, -0.35785694167464843, 0, 0.9337764236128835),
  scale: new Vector3(1, 1.1078691075823546, 1)
})
tree_Dead_03_17.addComponentOrReplace(transform_74)
engine.addEntity(tree_Dead_03_17)

const tree_Dead_01_22 = new Entity()
tree_Dead_01_22.setParent(scene)
tree_Dead_01_22.addComponentOrReplace(gltfShape_4)
const transform_75 = new Transform({
  position: new Vector3(30.5, 0, 57.5),
  rotation: new Quaternion(0, 0.9996561291910334, 0, 0.02622257368757171),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_22.addComponentOrReplace(transform_75)
engine.addEntity(tree_Dead_01_22)

const tree_Dead_01_23 = new Entity()
tree_Dead_01_23.setParent(scene)
tree_Dead_01_23.addComponentOrReplace(gltfShape_4)
const transform_76 = new Transform({
  position: new Vector3(47.32770680253024, 7.105427357601002e-15, 51.00804634676026),
  rotation: new Quaternion(0, -0.7426812061850063, 0, 0.6696451492989284),
  scale: new Vector3(1, 1.079415798674261, 1)
})
tree_Dead_01_23.addComponentOrReplace(transform_76)
engine.addEntity(tree_Dead_01_23)

const tree_Dead_03_18 = new Entity()
tree_Dead_03_18.setParent(scene)
tree_Dead_03_18.addComponentOrReplace(gltfShape_3)
const transform_77 = new Transform({
  position: new Vector3(42.06872251240353, 3.552713678800501e-15, 56.62648856258565),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1.150056368819524, 1)
})
tree_Dead_03_18.addComponentOrReplace(transform_77)
engine.addEntity(tree_Dead_03_18)

const tree_Dead_02_16 = new Entity()
tree_Dead_02_16.setParent(scene)
tree_Dead_02_16.addComponentOrReplace(gltfShape_5)
const transform_78 = new Transform({
  position: new Vector3(36.045005824049284, 7.105427357601002e-15, 55.836755716639004),
  rotation: new Quaternion(0, 0.9546806502073801, 0, 0.2976320818050601),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_16.addComponentOrReplace(transform_78)
engine.addEntity(tree_Dead_02_16)

const tree_Dead_03_19 = new Entity()
tree_Dead_03_19.setParent(scene)
tree_Dead_03_19.addComponentOrReplace(gltfShape_3)
const transform_79 = new Transform({
  position: new Vector3(55.21804831634068, 0, 22.112620633204138),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1.2379321945594945, 1)
})
tree_Dead_03_19.addComponentOrReplace(transform_79)
engine.addEntity(tree_Dead_03_19)

const tree_Dead_02_17 = new Entity()
tree_Dead_02_17.setParent(scene)
tree_Dead_02_17.addComponentOrReplace(gltfShape_5)
const transform_80 = new Transform({
  position: new Vector3(53.44469041391525, 0, 17.64888625064581),
  rotation: new Quaternion(0, -0.6762669649641844, 0, 0.7366566310691367),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_17.addComponentOrReplace(transform_80)
engine.addEntity(tree_Dead_02_17)

const tree_Dead_01_24 = new Entity()
tree_Dead_01_24.setParent(scene)
tree_Dead_01_24.addComponentOrReplace(gltfShape_4)
const transform_81 = new Transform({
  position: new Vector3(55.95774747323045, 7.105427357601002e-15, 35.476502733350785),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 0.7742627966813735, 1)
})
tree_Dead_01_24.addComponentOrReplace(transform_81)
engine.addEntity(tree_Dead_01_24)

const tree_Dead_02_18 = new Entity()
tree_Dead_02_18.setParent(scene)
tree_Dead_02_18.addComponentOrReplace(gltfShape_5)
const transform_82 = new Transform({
  position: new Vector3(55.25629126355663, 0, 30.66756394490214),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_18.addComponentOrReplace(transform_82)
engine.addEntity(tree_Dead_02_18)

const tree_Dead_01_25 = new Entity()
tree_Dead_01_25.setParent(scene)
tree_Dead_01_25.addComponentOrReplace(gltfShape_4)
const transform_83 = new Transform({
  position: new Vector3(51.267857581584856, 0, 47.336926322081176),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 0.8137722241744605, 1)
})
tree_Dead_01_25.addComponentOrReplace(transform_83)
engine.addEntity(tree_Dead_01_25)

const tree_Dead_02_19 = new Entity()
tree_Dead_02_19.setParent(scene)
tree_Dead_02_19.addComponentOrReplace(gltfShape_5)
const transform_84 = new Transform({
  position: new Vector3(54, 0, 43),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1.1112550182673218, 1)
})
tree_Dead_02_19.addComponentOrReplace(transform_84)
engine.addEntity(tree_Dead_02_19)

const tree_Dead_03_20 = new Entity()
tree_Dead_03_20.setParent(scene)
tree_Dead_03_20.addComponentOrReplace(gltfShape_3)
const transform_85 = new Transform({
  position: new Vector3(55.21119800241212, 0, 39.73068647910304),
  rotation: new Quaternion(0, 0.7285791838285234, 0, 0.6849615849752178),
  scale: new Vector3(1, 1.1535440327511388, 1)
})
tree_Dead_03_20.addComponentOrReplace(transform_85)
engine.addEntity(tree_Dead_03_20)

const tree_Dead_01_26 = new Entity()
tree_Dead_01_26.setParent(scene)
tree_Dead_01_26.addComponentOrReplace(gltfShape_4)
const transform_86 = new Transform({
  position: new Vector3(59.13211259622667, 3.552713678800501e-15, 19.78210238306942),
  rotation: new Quaternion(0, 0.22645271337259273, 0, 0.974022160223365),
  scale: new Vector3(1, 0.8833504639949439, 1)
})
tree_Dead_01_26.addComponentOrReplace(transform_86)
engine.addEntity(tree_Dead_01_26)

const tree_Dead_01_27 = new Entity()
tree_Dead_01_27.setParent(scene)
tree_Dead_01_27.addComponentOrReplace(gltfShape_4)
const transform_87 = new Transform({
  position: new Vector3(58.70762930274029, 0, 45.27079736631403),
  rotation: new Quaternion(0, -0.5912786872160142, 0, 0.8064673050062896),
  scale: new Vector3(1, 1.0362040142486713, 1)
})
tree_Dead_01_27.addComponentOrReplace(transform_87)
engine.addEntity(tree_Dead_01_27)

const tree_Dead_03_21 = new Entity()
tree_Dead_03_21.setParent(scene)
tree_Dead_03_21.addComponentOrReplace(gltfShape_3)
const transform_88 = new Transform({
  position: new Vector3(58.778676394899385, 0, 24.77850356692119),
  rotation: new Quaternion(0, -0.9893096139465122, 0, 0.14583033893193115),
  scale: new Vector3(1, 0.8655068999087696, 1)
})
tree_Dead_03_21.addComponentOrReplace(transform_88)
engine.addEntity(tree_Dead_03_21)

const tree_Dead_03_22 = new Entity()
tree_Dead_03_22.setParent(scene)
tree_Dead_03_22.addComponentOrReplace(gltfShape_3)
const transform_89 = new Transform({
  position: new Vector3(60.12869216330895, 7.105427357601002e-15, 33.875064650305134),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1.0553072846882294, 1)
})
tree_Dead_03_22.addComponentOrReplace(transform_89)
engine.addEntity(tree_Dead_03_22)

const tree_Dead_02_20 = new Entity()
tree_Dead_02_20.setParent(scene)
tree_Dead_02_20.addComponentOrReplace(gltfShape_5)
const transform_90 = new Transform({
  position: new Vector3(60.22154467919631, 0, 38.6581027796609),
  rotation: new Quaternion(0, 0.8621424047376458, 0, 0.5066660378920106),
  scale: new Vector3(1, 0.859316995812712, 1)
})
tree_Dead_02_20.addComponentOrReplace(transform_90)
engine.addEntity(tree_Dead_02_20)

const tree_Dead_02_21 = new Entity()
tree_Dead_02_21.setParent(scene)
tree_Dead_02_21.addComponentOrReplace(gltfShape_5)
const transform_91 = new Transform({
  position: new Vector3(55.45229771825599, 1.4210854715202004e-14, 50.70895210388706),
  rotation: new Quaternion(0, 0.584592271384843, 0, 0.8113272312927192),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_21.addComponentOrReplace(transform_91)
engine.addEntity(tree_Dead_02_21)

const tree_Dead_03_23 = new Entity()
tree_Dead_03_23.setParent(scene)
tree_Dead_03_23.addComponentOrReplace(gltfShape_3)
const transform_92 = new Transform({
  position: new Vector3(50.606685261316834, 1.4210854715202004e-14, 55.06305493082195),
  rotation: new Quaternion(0, 0.39062835969506454, 0, 0.9205484694473959),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_23.addComponentOrReplace(transform_92)
engine.addEntity(tree_Dead_03_23)

const tree_Dead_03_24 = new Entity()
tree_Dead_03_24.setParent(scene)
tree_Dead_03_24.addComponentOrReplace(gltfShape_3)
const transform_93 = new Transform({
  position: new Vector3(60.16196918328008, 0, 51.7889572426062),
  rotation: new Quaternion(0, 0.9468728953720782, 0, 0.32160802230307706),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_24.addComponentOrReplace(transform_93)
engine.addEntity(tree_Dead_03_24)

const tree_Dead_01_28 = new Entity()
tree_Dead_01_28.setParent(scene)
tree_Dead_01_28.addComponentOrReplace(gltfShape_4)
const transform_94 = new Transform({
  position: new Vector3(56.301921351413135, 7.105427357601002e-15, 57.23164558191614),
  rotation: new Quaternion(0, -0.647266506126144, 0, 0.762263779834287),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_28.addComponentOrReplace(transform_94)
engine.addEntity(tree_Dead_01_28)

const tree_Dead_01_29 = new Entity()
tree_Dead_01_29.setParent(scene)
tree_Dead_01_29.addComponentOrReplace(gltfShape_4)
const transform_95 = new Transform({
  position: new Vector3(46.51330836027328, 2.1316282072803006e-14, 59.235469775733854),
  rotation: new Quaternion(0, -0.9213603738143368, 0, 0.38870948221609675),
  scale: new Vector3(1, 1.0183083757629463, 1)
})
tree_Dead_01_29.addComponentOrReplace(transform_95)
engine.addEntity(tree_Dead_01_29)

const tree_Dead_03_25 = new Entity()
tree_Dead_03_25.setParent(scene)
tree_Dead_03_25.addComponentOrReplace(gltfShape_3)
const transform_96 = new Transform({
  position: new Vector3(38.744500297341474, 1.7763568394002505e-14, 60.059442243513644),
  rotation: new Quaternion(0, 0.3826834323650898, 0, 0.9238795325112867),
  scale: new Vector3(1, 0.8827669026607357, 1)
})
tree_Dead_03_25.addComponentOrReplace(transform_96)
engine.addEntity(tree_Dead_03_25)

const tree_Dead_01_30 = new Entity()
tree_Dead_01_30.setParent(scene)
tree_Dead_01_30.addComponentOrReplace(gltfShape_4)
const transform_97 = new Transform({
  position: new Vector3(22.327052324485813, 7.105427357601002e-15, 57.43206781277191),
  rotation: new Quaternion(0, 0.9548551192233248, 0, 0.2970718790007078),
  scale: new Vector3(1, 1.034128522704247, 1)
})
tree_Dead_01_30.addComponentOrReplace(transform_97)
engine.addEntity(tree_Dead_01_30)

const tree_Dead_02_22 = new Entity()
tree_Dead_02_22.setParent(scene)
tree_Dead_02_22.addComponentOrReplace(gltfShape_5)
const transform_98 = new Transform({
  position: new Vector3(33.894979888351, 7.105427357601002e-15, 60.540171408698946),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1.2139052237046735, 1)
})
tree_Dead_02_22.addComponentOrReplace(transform_98)
engine.addEntity(tree_Dead_02_22)

const tree_Dead_03_26 = new Entity()
tree_Dead_03_26.setParent(scene)
tree_Dead_03_26.addComponentOrReplace(gltfShape_3)
const transform_99 = new Transform({
  position: new Vector3(27.56379666327882, 0, 60.909023458048004),
  rotation: new Quaternion(0, 0.7971652717180485, 0, 0.603761152747251),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_26.addComponentOrReplace(transform_99)
engine.addEntity(tree_Dead_03_26)

const tree_Dead_02_23 = new Entity()
tree_Dead_02_23.setParent(scene)
tree_Dead_02_23.addComponentOrReplace(gltfShape_5)
const transform_100 = new Transform({
  position: new Vector3(17.382410150175108, 0, 58.184848463430036),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_23.addComponentOrReplace(transform_100)
engine.addEntity(tree_Dead_02_23)

const tree_Dead_03_27 = new Entity()
tree_Dead_03_27.setParent(scene)
tree_Dead_03_27.addComponentOrReplace(gltfShape_3)
const transform_101 = new Transform({
  position: new Vector3(9.595549252840756, 0, 50.59486342505474),
  rotation: new Quaternion(0, 0.47791698104873104, 0, 0.8784050086521975),
  scale: new Vector3(1, 1.0487700518161454, 1)
})
tree_Dead_03_27.addComponentOrReplace(transform_101)
engine.addEntity(tree_Dead_03_27)

const tree_Dead_01_31 = new Entity()
tree_Dead_01_31.setParent(scene)
tree_Dead_01_31.addComponentOrReplace(gltfShape_4)
const transform_102 = new Transform({
  position: new Vector3(12.246444095086193, 0, 55.1042333680423),
  rotation: new Quaternion(0, 0.9951847266721968, 0, -0.09801714032956059),
  scale: new Vector3(1, 0.8504125758897239, 1)
})
tree_Dead_01_31.addComponentOrReplace(transform_102)
engine.addEntity(tree_Dead_01_31)

const tree_Dead_02_24 = new Entity()
tree_Dead_02_24.setParent(scene)
tree_Dead_02_24.addComponentOrReplace(gltfShape_5)
const transform_103 = new Transform({
  position: new Vector3(3.932506736536677, 0, 51.97275955862705),
  rotation: new Quaternion(0, 0.9264041161911631, 0, 0.37653076036901817),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_24.addComponentOrReplace(transform_103)
engine.addEntity(tree_Dead_02_24)

const tree_Dead_03_28 = new Entity()
tree_Dead_03_28.setParent(scene)
tree_Dead_03_28.addComponentOrReplace(gltfShape_3)
const transform_104 = new Transform({
  position: new Vector3(6.499289988527109, 7.105427357601002e-15, 56.36901730470645),
  rotation: new Quaternion(0, -0.9068829807768625, 0, 0.42138255680233405),
  scale: new Vector3(1, 1.1572333669798862, 1)
})
tree_Dead_03_28.addComponentOrReplace(transform_104)
engine.addEntity(tree_Dead_03_28)

const tree_Dead_03_29 = new Entity()
tree_Dead_03_29.setParent(scene)
tree_Dead_03_29.addComponentOrReplace(gltfShape_3)
const transform_105 = new Transform({
  position: new Vector3(12, 0, 60.5),
  rotation: new Quaternion(0, 0.8434646111835442, 0, 0.5371847444604071),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_29.addComponentOrReplace(transform_105)
engine.addEntity(tree_Dead_03_29)

const tree_Dead_03_30 = new Entity()
tree_Dead_03_30.setParent(scene)
tree_Dead_03_30.addComponentOrReplace(gltfShape_3)
const transform_106 = new Transform({
  position: new Vector3(3.1360012685364396, 0, 43.250372900987955),
  rotation: new Quaternion(0, -0.513537161021953, 0, 0.8580673541450647),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_30.addComponentOrReplace(transform_106)
engine.addEntity(tree_Dead_03_30)

const tree_Dead_01_32 = new Entity()
tree_Dead_01_32.setParent(scene)
tree_Dead_01_32.addComponentOrReplace(gltfShape_4)
const transform_107 = new Transform({
  position: new Vector3(56.047694667922876, 7.105427357601002e-15, 14.048493765062801),
  rotation: new Quaternion(0, 0.5533020356085996, 0, 0.8329807064940824),
  scale: new Vector3(1, 1.1399254822093092, 1)
})
tree_Dead_01_32.addComponentOrReplace(transform_107)
engine.addEntity(tree_Dead_01_32)

const tree_Dead_01_33 = new Entity()
tree_Dead_01_33.setParent(scene)
tree_Dead_01_33.addComponentOrReplace(gltfShape_4)
const transform_108 = new Transform({
  position: new Vector3(46.55968703327798, 0, 5.2595384224228),
  rotation: new Quaternion(0.002380923427658308, 0, 0, 0.999997165597799),
  scale: new Vector3(1, 1.1209777613357184, 1)
})
tree_Dead_01_33.addComponentOrReplace(transform_108)
engine.addEntity(tree_Dead_01_33)

const tree_Dead_02_25 = new Entity()
tree_Dead_02_25.setParent(scene)
tree_Dead_02_25.addComponentOrReplace(gltfShape_5)
const transform_109 = new Transform({
  position: new Vector3(52.45840640360025, 0.0015629400967327456, 7.45713877984727),
  rotation: new Quaternion(0, -0.24292196924921303, 0, 0.9700458323481856),
  scale: new Vector3(1, 1.3356978563071475, 1)
})
tree_Dead_02_25.addComponentOrReplace(transform_109)
engine.addEntity(tree_Dead_02_25)

const tree_Dead_03_31 = new Entity()
tree_Dead_03_31.setParent(scene)
tree_Dead_03_31.addComponentOrReplace(gltfShape_3)
const transform_110 = new Transform({
  position: new Vector3(55.98317886102907, 0, 9.346719375540818),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_31.addComponentOrReplace(transform_110)
engine.addEntity(tree_Dead_03_31)

const tree_Dead_01_34 = new Entity()
tree_Dead_01_34.setParent(scene)
tree_Dead_01_34.addComponentOrReplace(gltfShape_4)
const transform_111 = new Transform({
  position: new Vector3(57.009813322008156, 7.105427357601002e-15, 3.7379377430450997),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 0.9130331021689635, 1)
})
tree_Dead_01_34.addComponentOrReplace(transform_111)
engine.addEntity(tree_Dead_01_34)

const tree_Dead_02_26 = new Entity()
tree_Dead_02_26.setParent(scene)
tree_Dead_02_26.addComponentOrReplace(gltfShape_5)
const transform_112 = new Transform({
  position: new Vector3(61.25561082647755, 0, 10.986084692020206),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_26.addComponentOrReplace(transform_112)
engine.addEntity(tree_Dead_02_26)

const tree_Dead_03_32 = new Entity()
tree_Dead_03_32.setParent(scene)
tree_Dead_03_32.addComponentOrReplace(gltfShape_3)
const transform_113 = new Transform({
  position: new Vector3(3.656924262784711, 0, 18.59678650220948),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 0.9132171369237305, 1)
})
tree_Dead_03_32.addComponentOrReplace(transform_113)
engine.addEntity(tree_Dead_03_32)

const tree_Dead_01_35 = new Entity()
tree_Dead_01_35.setParent(scene)
tree_Dead_01_35.addComponentOrReplace(gltfShape_4)
const transform_114 = new Transform({
  position: new Vector3(4.667577128153845, 0, 14.031710259211422),
  rotation: new Quaternion(0, -0.7903762350022991, 0, 0.6126217488333161),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01_35.addComponentOrReplace(transform_114)
engine.addEntity(tree_Dead_01_35)

const tree_Dead_02_27 = new Entity()
tree_Dead_02_27.setParent(scene)
tree_Dead_02_27.addComponentOrReplace(gltfShape_5)
const transform_115 = new Transform({
  position: new Vector3(4.131136406190214, 0, 4.546166992660669),
  rotation: new Quaternion(0, -0.4615797127317383, 0, 0.887098736778768),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02_27.addComponentOrReplace(transform_115)
engine.addEntity(tree_Dead_02_27)

const tree_Dead_03_33 = new Entity()
tree_Dead_03_33.setParent(scene)
tree_Dead_03_33.addComponentOrReplace(gltfShape_3)
const transform_116 = new Transform({
  position: new Vector3(9.972703961079525, 0, 12.201749934579404),
  rotation: new Quaternion(0, 0.4639632995014217, 0, 0.8858544218525718),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_33.addComponentOrReplace(transform_116)
engine.addEntity(tree_Dead_03_33)

const tree_Dead_01_36 = new Entity()
tree_Dead_01_36.setParent(scene)
tree_Dead_01_36.addComponentOrReplace(gltfShape_4)
const transform_117 = new Transform({
  position: new Vector3(14.458595168905926, 0, 4.017822969345546),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 0.8988843475246924, 1)
})
tree_Dead_01_36.addComponentOrReplace(transform_117)
engine.addEntity(tree_Dead_01_36)

const tree_Dead_03_34 = new Entity()
tree_Dead_03_34.setParent(scene)
tree_Dead_03_34.addComponentOrReplace(gltfShape_3)
const transform_118 = new Transform({
  position: new Vector3(8.922729500078981, 0, 3.4544931991438097),
  rotation: new Quaternion(0, -0.9238828791765524, 0, 0.3826753527004888),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_03_34.addComponentOrReplace(transform_118)
engine.addEntity(tree_Dead_03_34)

const tree_Dead_03_35 = new Entity()
tree_Dead_03_35.setParent(scene)
tree_Dead_03_35.addComponentOrReplace(gltfShape_3)
const transform_119 = new Transform({
  position: new Vector3(5.880190067191868, 1.4210854715202004e-14, 8.38115365397901),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1.2560369460249685, 1)
})
tree_Dead_03_35.addComponentOrReplace(transform_119)
engine.addEntity(tree_Dead_03_35)

const tree_Dead_01_37 = new Entity()
tree_Dead_01_37.setParent(scene)
tree_Dead_01_37.addComponentOrReplace(gltfShape_4)
const transform_120 = new Transform({
  position: new Vector3(11.769809103241652, 1.4210854715202004e-14, 8.143981257062789),
  rotation: new Quaternion(0, -0.7037530756357411, 0, 0.7104446555033231),
  scale: new Vector3(1, 1.0402266149130437, 1)
})
tree_Dead_01_37.addComponentOrReplace(transform_120)
engine.addEntity(tree_Dead_01_37)

const torch_01 = new Entity()
torch_01.setParent(scene)
const gltfShape_6 = new GLTFShape('models/Torch_01/Torch_01.glb')
torch_01.addComponentOrReplace(gltfShape_6)
const transform_121 = new Transform({
  position: new Vector3(32.24591371739473, 2.901328600309543, 7.248032028301533),
  rotation: new Quaternion(-0.026778760792764505, 0.03489050217897386, 0.36462487326452486, 0.9301151824505027),
  scale: new Vector3(1, 1, 1)
})
torch_01.addComponentOrReplace(transform_121)
engine.addEntity(torch_01)

const torch_01_2 = new Entity()
torch_01_2.setParent(scene)
torch_01_2.addComponentOrReplace(gltfShape_6)
const transform_122 = new Transform({
  position: new Vector3(29.853145375426124, 2.938927954910315, 7.73081179115245),
  rotation: new Quaternion(-0.10704316757196113, 0.23514235193151523, -0.4002503679122062, 0.8792323228762811),
  scale: new Vector3(1, 1, 1)
})
torch_01_2.addComponentOrReplace(transform_122)
engine.addEntity(torch_01_2)

const rockFloor_Module_4M = new Entity()
rockFloor_Module_4M.setParent(scene)
const gltfShape_7 = new GLTFShape('models/RockFloor_Module_4M/RockFloor_Module_4M.glb')
rockFloor_Module_4M.addComponentOrReplace(gltfShape_7)
const transform_123 = new Transform({
  position: new Vector3(28.96316286866056, 7.105427357601002e-15, 2.7668315720104975),
  rotation: new Quaternion(0, 0.9998853814647739, 0, 0.015140143098518064),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_4M.addComponentOrReplace(transform_123)
engine.addEntity(rockFloor_Module_4M)

const rockFloor_Module_4M_2 = new Entity()
rockFloor_Module_4M_2.setParent(scene)
rockFloor_Module_4M_2.addComponentOrReplace(gltfShape_7)
const transform_124 = new Transform({
  position: new Vector3(33.1505604975857, 3.552713678800501e-15, 12.212926110826338),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_4M_2.addComponentOrReplace(transform_124)
engine.addEntity(rockFloor_Module_4M_2)

const rockFloor_Module_4M_3 = new Entity()
rockFloor_Module_4M_3.setParent(scene)
rockFloor_Module_4M_3.addComponentOrReplace(gltfShape_7)
const transform_125 = new Transform({
  position: new Vector3(33.52493784924774, 0.00005525328652922212, 16.166266992640374),
  rotation: new Quaternion(-0.0011671860124444996, 0.015769663243791813, -0.00020565552147576934, 0.9998749487329879),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_4M_3.addComponentOrReplace(transform_125)
engine.addEntity(rockFloor_Module_4M_3)

const rockFloor_Module_4M_4 = new Entity()
rockFloor_Module_4M_4.setParent(scene)
rockFloor_Module_4M_4.addComponentOrReplace(gltfShape_7)
const transform_126 = new Transform({
  position: new Vector3(33.732629504603345, 7.105427357601002e-15, 19.940199822416798),
  rotation: new Quaternion(0, 0.055937668153835315, 0, 0.9984342628743825),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_4M_4.addComponentOrReplace(transform_126)
engine.addEntity(rockFloor_Module_4M_4)

const rockFloor_Module_4M_5 = new Entity()
rockFloor_Module_4M_5.setParent(scene)
rockFloor_Module_4M_5.addComponentOrReplace(gltfShape_7)
const transform_127 = new Transform({
  position: new Vector3(29.520290183366924, 1.4210854715202004e-14, 20.279861576774643),
  rotation: new Quaternion(0, 0.9969377160627033, 0, -0.07819968217122233),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_4M_5.addComponentOrReplace(transform_127)
engine.addEntity(rockFloor_Module_4M_5)

const rockFloor_Module_2M = new Entity()
rockFloor_Module_2M.setParent(scene)
const gltfShape_8 = new GLTFShape('models/RockFloor_Module_2M/RockFloor_Module_2M.glb')
rockFloor_Module_2M.addComponentOrReplace(gltfShape_8)
const transform_128 = new Transform({
  position: new Vector3(33.31701673904029, 1.2434497875801753e-14, 25.129422448797925),
  rotation: new Quaternion(0, 0.4906769571069039, 0, 0.8713415654978879),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_2M.addComponentOrReplace(transform_128)
engine.addEntity(rockFloor_Module_2M)

const rockFloor_Module_1M = new Entity()
rockFloor_Module_1M.setParent(scene)
const gltfShape_9 = new GLTFShape('models/RockFloor_Module_1M/RockFloor_Module_1M.glb')
rockFloor_Module_1M.addComponentOrReplace(gltfShape_9)
const transform_129 = new Transform({
  position: new Vector3(33.15482254423506, 0, 24.819300184375944),
  rotation: new Quaternion(0, -0.31933485680741663, 0, 0.947641941467233),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M.addComponentOrReplace(transform_129)
engine.addEntity(rockFloor_Module_1M)

const rockFloor_Module_1M_2 = new Entity()
rockFloor_Module_1M_2.setParent(scene)
rockFloor_Module_1M_2.addComponentOrReplace(gltfShape_9)
const transform_130 = new Transform({
  position: new Vector3(31.189192184542925, 0, 24.91712847270661),
  rotation: new Quaternion(0, 0.24729227118017805, 0, 0.9689409335013923),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_2.addComponentOrReplace(transform_130)
engine.addEntity(rockFloor_Module_1M_2)

const templeMoon_01 = new Entity()
templeMoon_01.setParent(scene)
const gltfShape_10 = new GLTFShape('models/TempleMoon_01/TempleMoon_01.glb')
templeMoon_01.addComponentOrReplace(gltfShape_10)
const transform_131 = new Transform({
  position: new Vector3(32.2962004851459, 3.552713678800501e-15, 48.14685930683064),
  rotation: new Quaternion(0, -0.9999999999999999, 0, 2.220446049250313e-16),
  scale: new Vector3(1, 1, 1)
})
templeMoon_01.addComponentOrReplace(transform_131)
engine.addEntity(templeMoon_01)

const rockFloor_Module_4M_6 = new Entity()
rockFloor_Module_4M_6.setParent(scene)
rockFloor_Module_4M_6.addComponentOrReplace(gltfShape_7)
const transform_132 = new Transform({
  position: new Vector3(43.80920214302757, 0.042569432630816806, 27.131576054319698),
  rotation: new Quaternion(0, 0.9983122903334749, 0, -0.05807384066110993),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_4M_6.addComponentOrReplace(transform_132)
engine.addEntity(rockFloor_Module_4M_6)

const rockFloor_Module_4M_7 = new Entity()
rockFloor_Module_4M_7.setParent(scene)
rockFloor_Module_4M_7.addComponentOrReplace(gltfShape_7)
const transform_133 = new Transform({
  position: new Vector3(43.98822640982281, 3.552713678800501e-15, 31.10896725260374),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_4M_7.addComponentOrReplace(transform_133)
engine.addEntity(rockFloor_Module_4M_7)

const rockFloor_Module_4M_8 = new Entity()
rockFloor_Module_4M_8.setParent(scene)
rockFloor_Module_4M_8.addComponentOrReplace(gltfShape_7)
const transform_134 = new Transform({
  position: new Vector3(39.861411408991394, 0, 30.906191280946512),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_4M_8.addComponentOrReplace(transform_134)
engine.addEntity(rockFloor_Module_4M_8)

const rockFloor_Module_2M_2 = new Entity()
rockFloor_Module_2M_2.setParent(scene)
rockFloor_Module_2M_2.addComponentOrReplace(gltfShape_8)
const transform_135 = new Transform({
  position: new Vector3(36.09631915086837, 5.329070518200751e-15, 29.594898696244584),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_2M_2.addComponentOrReplace(transform_135)
engine.addEntity(rockFloor_Module_2M_2)

const pedestal_01 = new Entity()
pedestal_01.setParent(scene)
const gltfShape_12 = new GLTFShape('models/Pedestal_01/Pedestal_01.glb')
pedestal_01.addComponentOrReplace(gltfShape_12)
const transform_137 = new Transform({
  position: new Vector3(12, 0, 29.267169192084513),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
pedestal_01.addComponentOrReplace(transform_137)
engine.addEntity(pedestal_01)

const pedestal_01_2 = new Entity()
pedestal_01_2.setParent(scene)
pedestal_01_2.addComponentOrReplace(gltfShape_12)
const transform_139 = new Transform({
  position: new Vector3(50.07622849590735, 0, 28.840837970076308),
  rotation: new Quaternion(0, 0.7006858664946777, 0, -0.7134699128166537),
  scale: new Vector3(1, 1, 1)
})
pedestal_01_2.addComponentOrReplace(transform_139)
engine.addEntity(pedestal_01_2)

const tree_Dead_01_38 = new Entity()
tree_Dead_01_38.setParent(scene)
tree_Dead_01_38.addComponentOrReplace(gltfShape_4)
const transform_140 = new Transform({
  position: new Vector3(58.58147173839182, 0, 28.75388859419051),
  rotation: new Quaternion(0, -0.69290447251345, 0, 0.7210293974387304),
  scale: new Vector3(1, 1.2177794962503619, 1)
})
tree_Dead_01_38.addComponentOrReplace(transform_140)
engine.addEntity(tree_Dead_01_38)

const rockFloor_Module_4M_9 = new Entity()
rockFloor_Module_4M_9.setParent(scene)
rockFloor_Module_4M_9.addComponentOrReplace(gltfShape_7)
const transform_143 = new Transform({
  position: new Vector3(33.98979313003725, 0, 37.70765988343584),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_4M_9.addComponentOrReplace(transform_143)
engine.addEntity(rockFloor_Module_4M_9)

const rockFloor_Module_4M_10 = new Entity()
rockFloor_Module_4M_10.setParent(scene)
rockFloor_Module_4M_10.addComponentOrReplace(gltfShape_7)
const transform_144 = new Transform({
  position: new Vector3(29.93773760319635, 3.552713678800501e-15, 41.97391918686375),
  rotation: new Quaternion(0, 0.9985501246276153, 0, -0.05382981150044519),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_4M_10.addComponentOrReplace(transform_144)
engine.addEntity(rockFloor_Module_4M_10)

const rockFloor_Module_4M_11 = new Entity()
rockFloor_Module_4M_11.setParent(scene)
rockFloor_Module_4M_11.addComponentOrReplace(gltfShape_7)
const transform_145 = new Transform({
  position: new Vector3(34.06154245731702, 0, 41.77213273063772),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_4M_11.addComponentOrReplace(transform_145)
engine.addEntity(rockFloor_Module_4M_11)

const rockFloor_Module_2M_3 = new Entity()
rockFloor_Module_2M_3.setParent(scene)
rockFloor_Module_2M_3.addComponentOrReplace(gltfShape_8)
const transform_146 = new Transform({
  position: new Vector3(33.01418933568167, 0, 33.11097780378913),
  rotation: new Quaternion(0, 0.20230649813123228, 0, 0.9793222558554857),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_2M_3.addComponentOrReplace(transform_146)
engine.addEntity(rockFloor_Module_2M_3)

const rockFloor_Module_1M_3 = new Entity()
rockFloor_Module_1M_3.setParent(scene)
rockFloor_Module_1M_3.addComponentOrReplace(gltfShape_9)
const transform_147 = new Transform({
  position: new Vector3(31.65918838401973, 3.552713678800501e-15, 31.58721987965792),
  rotation: new Quaternion(0, -0.6630320576276839, 0, 0.7485910035246213),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_3.addComponentOrReplace(transform_147)
engine.addEntity(rockFloor_Module_1M_3)

const rockFloor_Module_4M_12 = new Entity()
rockFloor_Module_4M_12.setParent(scene)
rockFloor_Module_4M_12.addComponentOrReplace(gltfShape_7)
const transform_148 = new Transform({
  position: new Vector3(18.519049680816597, 0, 31.26805954657577),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_4M_12.addComponentOrReplace(transform_148)
engine.addEntity(rockFloor_Module_4M_12)

const rockFloor_Module_4M_13 = new Entity()
rockFloor_Module_4M_13.setParent(scene)
rockFloor_Module_4M_13.addComponentOrReplace(gltfShape_7)
const transform_149 = new Transform({
  position: new Vector3(22.720588617274583, 0, 26.847347599402283),
  rotation: new Quaternion(0, 0.7380823391183414, 0, 0.6747106495984764),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_4M_13.addComponentOrReplace(transform_149)
engine.addEntity(rockFloor_Module_4M_13)

const rockFloor_Module_4M_14 = new Entity()
rockFloor_Module_4M_14.setParent(scene)
rockFloor_Module_4M_14.addComponentOrReplace(gltfShape_7)
const transform_150 = new Transform({
  position: new Vector3(23.12452292802444, 3.552713678800501e-15, 31.155951941685114),
  rotation: new Quaternion(0, -0.7071067811865478, 0, 0.7071067811865478),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_4M_14.addComponentOrReplace(transform_150)
engine.addEntity(rockFloor_Module_4M_14)

const rockFloor_Module_2M_4 = new Entity()
rockFloor_Module_2M_4.setParent(scene)
rockFloor_Module_2M_4.addComponentOrReplace(gltfShape_8)
const transform_151 = new Transform({
  position: new Vector3(27.535069375164664, 0, 30.294892040404036),
  rotation: new Quaternion(0, -0.6566001952735285, 0, 0.7542388107004071),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_2M_4.addComponentOrReplace(transform_151)
engine.addEntity(rockFloor_Module_2M_4)

const stone_02 = new Entity()
stone_02.setParent(scene)
const gltfShape_15 = new GLTFShape('models/Stone_02/Stone_02.glb')
stone_02.addComponentOrReplace(gltfShape_15)
const transform_152 = new Transform({
  position: new Vector3(33.623971476793294, 0, 50.09068770033645),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
stone_02.addComponentOrReplace(transform_152)
engine.addEntity(stone_02)

const stone_02_2 = new Entity()
stone_02_2.setParent(scene)
stone_02_2.addComponentOrReplace(gltfShape_15)
const transform_153 = new Transform({
  position: new Vector3(31.162051811992665, 1.7763568394002505e-15, 50.24408469345838),
  rotation: new Quaternion(0, 0.9909289918592967, 0, 0.13438650636398985),
  scale: new Vector3(1, 1, 1)
})
stone_02_2.addComponentOrReplace(transform_153)
engine.addEntity(stone_02_2)

const plant_02 = new Entity()
plant_02.setParent(scene)
const gltfShape_16 = new GLTFShape('models/Plant_02/Plant_02.glb')
plant_02.addComponentOrReplace(gltfShape_16)
const transform_154 = new Transform({
  position: new Vector3(32.35314817347697, 0.6326091620306489, 48.46859360090763),
  rotation: new Quaternion(0, 0.7967358949504701, 0, 0.6043276542551002),
  scale: new Vector3(1, 1, 1)
})
plant_02.addComponentOrReplace(transform_154)
engine.addEntity(plant_02)

const plant_03 = new Entity()
plant_03.setParent(scene)
const gltfShape_17 = new GLTFShape('models/Plant_03/Plant_03.glb')
plant_03.addComponentOrReplace(gltfShape_17)
const transform_155 = new Transform({
  position: new Vector3(32.361695077925326, 0.5009946930528777, 48.4747329440843),
  rotation: new Quaternion(0.01807639471573221, 0.9364956030858136, -0.12955607957928986, -0.32536817852866584),
  scale: new Vector3(1, 1, 1)
})
plant_03.addComponentOrReplace(transform_155)
engine.addEntity(plant_03)

const rockFloor_Module_2M_5 = new Entity()
rockFloor_Module_2M_5.setParent(scene)
rockFloor_Module_2M_5.addComponentOrReplace(gltfShape_8)
const transform_157 = new Transform({
  position: new Vector3(33.20206626616795, 3.552713678800501e-15, 28.711691426910335),
  rotation: new Quaternion(0, -0.380697137098344, 0, 0.9246997836082393),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_2M_5.addComponentOrReplace(transform_157)
engine.addEntity(rockFloor_Module_2M_5)

const rockFloor_Module_2M_6 = new Entity()
rockFloor_Module_2M_6.setParent(scene)
rockFloor_Module_2M_6.addComponentOrReplace(gltfShape_8)
const transform_158 = new Transform({
  position: new Vector3(30.939060793226137, 0, 28.58303304453148),
  rotation: new Quaternion(0, -0.2525307760812484, 0, 0.967588862653866),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_2M_6.addComponentOrReplace(transform_158)
engine.addEntity(rockFloor_Module_2M_6)

const rockFloor_Module_2M_7 = new Entity()
rockFloor_Module_2M_7.setParent(scene)
rockFloor_Module_2M_7.addComponentOrReplace(gltfShape_8)
const transform_159 = new Transform({
  position: new Vector3(33.49748382785434, 0, 28.73429931197697),
  rotation: new Quaternion(0, 0.921370704117944, 0, 0.3886849953281002),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_2M_7.addComponentOrReplace(transform_159)
engine.addEntity(rockFloor_Module_2M_7)

const rockFloor_Module_2M_8 = new Entity()
rockFloor_Module_2M_8.setParent(scene)
rockFloor_Module_2M_8.addComponentOrReplace(gltfShape_8)
const transform_160 = new Transform({
  position: new Vector3(31.98900857226565, 3.552713678800501e-15, 30.19116516885799),
  rotation: new Quaternion(0, 0.3553174086292897, 0, 0.9347457082677438),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_2M_8.addComponentOrReplace(transform_160)
engine.addEntity(rockFloor_Module_2M_8)

const rockFloor_Module_1M_4 = new Entity()
rockFloor_Module_1M_4.setParent(scene)
rockFloor_Module_1M_4.addComponentOrReplace(gltfShape_9)
const transform_161 = new Transform({
  position: new Vector3(29.643605304295573, 0, 29.092333379387362),
  rotation: new Quaternion(0, -0.6471738203171403, 0, 0.7623424731025532),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_4.addComponentOrReplace(transform_161)
engine.addEntity(rockFloor_Module_1M_4)

const rockFloor_Module_1M_5 = new Entity()
rockFloor_Module_1M_5.setParent(scene)
rockFloor_Module_1M_5.addComponentOrReplace(gltfShape_9)
const transform_162 = new Transform({
  position: new Vector3(34.16048900726211, 1.0658141036401503e-14, 29.156165925384435),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_5.addComponentOrReplace(transform_162)
engine.addEntity(rockFloor_Module_1M_5)

const rockFloor_Module_1M_6 = new Entity()
rockFloor_Module_1M_6.setParent(scene)
rockFloor_Module_1M_6.addComponentOrReplace(gltfShape_9)
const transform_163 = new Transform({
  position: new Vector3(33.66757354822673, 0, 31.839705607492547),
  rotation: new Quaternion(0, 0.25124917378465744, 0, 0.9679224414551653),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_6.addComponentOrReplace(transform_163)
engine.addEntity(rockFloor_Module_1M_6)

const rockFloor_Module_1M_7 = new Entity()
rockFloor_Module_1M_7.setParent(scene)
rockFloor_Module_1M_7.addComponentOrReplace(gltfShape_9)
const transform_164 = new Transform({
  position: new Vector3(31.5, 0, 32),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_7.addComponentOrReplace(transform_164)
engine.addEntity(rockFloor_Module_1M_7)

const rockFloor_Module_1M_8 = new Entity()
rockFloor_Module_1M_8.setParent(scene)
rockFloor_Module_1M_8.addComponentOrReplace(gltfShape_9)
const transform_165 = new Transform({
  position: new Vector3(33.00213230082885, 0, 5.198640014715666),
  rotation: new Quaternion(0, -0.6611904125095108, 0, 0.7502181272173464),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_8.addComponentOrReplace(transform_165)
engine.addEntity(rockFloor_Module_1M_8)

const rockFloor_Module_1M_9 = new Entity()
rockFloor_Module_1M_9.setParent(scene)
rockFloor_Module_1M_9.addComponentOrReplace(gltfShape_9)
const transform_166 = new Transform({
  position: new Vector3(29.186758075162263, 1.7763568394002505e-15, 4.048486282491421),
  rotation: new Quaternion(0, 0.11201477019223947, 0, 0.9937065418214674),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_9.addComponentOrReplace(transform_166)
engine.addEntity(rockFloor_Module_1M_9)

const rockFloor_Module_1M_10 = new Entity()
rockFloor_Module_1M_10.setParent(scene)
rockFloor_Module_1M_10.addComponentOrReplace(gltfShape_9)
const transform_167 = new Transform({
  position: new Vector3(28.86901912634329, 0, 4.5443453859173575),
  rotation: new Quaternion(0, 0.6448743104342121, 0, 0.7642886390245494),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_10.addComponentOrReplace(transform_167)
engine.addEntity(rockFloor_Module_1M_10)

const rockFloor_Module_1M_11 = new Entity()
rockFloor_Module_1M_11.setParent(scene)
rockFloor_Module_1M_11.addComponentOrReplace(gltfShape_9)
const transform_168 = new Transform({
  position: new Vector3(32.69688330129923, 1.7763568394002505e-15, 2.011118749178787),
  rotation: new Quaternion(0, 0.9991766076052071, 0, -0.040572241921662644),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_11.addComponentOrReplace(transform_168)
engine.addEntity(rockFloor_Module_1M_11)

const rockFloor_Module_1M_12 = new Entity()
rockFloor_Module_1M_12.setParent(scene)
rockFloor_Module_1M_12.addComponentOrReplace(gltfShape_9)
const transform_169 = new Transform({
  position: new Vector3(32.17894909990941, 7.105427357601002e-15, 3.0749583053791283),
  rotation: new Quaternion(0, -0.35169224858221854, 0, 0.9361156778343064),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_12.addComponentOrReplace(transform_169)
engine.addEntity(rockFloor_Module_1M_12)

const rockFloor_Module_1M_13 = new Entity()
rockFloor_Module_1M_13.setParent(scene)
rockFloor_Module_1M_13.addComponentOrReplace(gltfShape_9)
const transform_170 = new Transform({
  position: new Vector3(34.24477186029201, 0, 3.2533592626165495),
  rotation: new Quaternion(0, 0.5493959832854579, 0, 0.8355621183070734),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_13.addComponentOrReplace(transform_170)
engine.addEntity(rockFloor_Module_1M_13)

const rockFloor_Module_1M_14 = new Entity()
rockFloor_Module_1M_14.setParent(scene)
rockFloor_Module_1M_14.addComponentOrReplace(gltfShape_9)
const transform_171 = new Transform({
  position: new Vector3(29.40395839045778, 3.552713678800501e-15, 1.149928843814449),
  rotation: new Quaternion(0, 0.674364502038593, 0, 0.7383986175435601),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_14.addComponentOrReplace(transform_171)
engine.addEntity(rockFloor_Module_1M_14)

const rockFloor_Module_2M_9 = new Entity()
rockFloor_Module_2M_9.setParent(scene)
rockFloor_Module_2M_9.addComponentOrReplace(gltfShape_8)
const transform_172 = new Transform({
  position: new Vector3(29.73559497044274, 3.552713678800501e-15, 0.8251370602710275),
  rotation: new Quaternion(0, 0.9912763636585719, 0, 0.13179973767742656),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_2M_9.addComponentOrReplace(transform_172)
engine.addEntity(rockFloor_Module_2M_9)

const rockFloor_Module_1M_15 = new Entity()
rockFloor_Module_1M_15.setParent(scene)
rockFloor_Module_1M_15.addComponentOrReplace(gltfShape_9)
const transform_173 = new Transform({
  position: new Vector3(28.01307519459941, 1.7763568394002505e-15, 3.236493481720226),
  rotation: new Quaternion(0, -0.6212187223215474, 0, 0.7836372241268172),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_15.addComponentOrReplace(transform_173)
engine.addEntity(rockFloor_Module_1M_15)

const rockFloor_Module_1M_16 = new Entity()
rockFloor_Module_1M_16.setParent(scene)
rockFloor_Module_1M_16.addComponentOrReplace(gltfShape_9)
const transform_174 = new Transform({
  position: new Vector3(32.60761341915632, 0, 0.7714861909036586),
  rotation: new Quaternion(0, 0.8900825693771434, 0, 0.45579931953764674),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_16.addComponentOrReplace(transform_174)
engine.addEntity(rockFloor_Module_1M_16)

const rockFloor_Module_1M_17 = new Entity()
rockFloor_Module_1M_17.setParent(scene)
rockFloor_Module_1M_17.addComponentOrReplace(gltfShape_9)
const transform_175 = new Transform({
  position: new Vector3(34.584973087181616, 0, 5.338344889446222),
  rotation: new Quaternion(0, 0.7157315938571008, 0, 0.6983754617358591),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_17.addComponentOrReplace(transform_175)
engine.addEntity(rockFloor_Module_1M_17)

const rockFloor_Module_1M_18 = new Entity()
rockFloor_Module_1M_18.setParent(scene)
rockFloor_Module_1M_18.addComponentOrReplace(gltfShape_9)
const transform_176 = new Transform({
  position: new Vector3(27.74676218716209, 1.7763568394002505e-15, 5.301352459414483),
  rotation: new Quaternion(0, 0.942515948291573, 0, 0.3341611695215898),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_1M_18.addComponentOrReplace(transform_176)
engine.addEntity(rockFloor_Module_1M_18)


/////////// Ambient Music from the Altar
const musicClip = new AudioClip('music/Lost_In_Prayer.mp3')
const musicSource = new AudioSource(musicClip)
plant_02.addComponent(musicSource)
musicSource.playing = playMusic
musicSource.loop = true
musicSource.volume = 0.3


//const hud = new BuilderHUD()
//hud.attachToEntity(plant_02)


